package fr.tr_pl.web_semantique_nlp;

import fr.tr_pl.web_semantique_nlp.search.Search;

import java.io.IOException;
import java.util.Scanner;

public class Main
{
  
  public static void main(String[] args)
  {
    Search  search_module = new Search();
    Scanner scanner       = new Scanner(System.in);
    while (true)
    {
      System.out.print("\nWhat is your research ? ");
      String input = scanner.nextLine();
      //System.out.println(input);
      String search_result = null;
      try
      {
        search_result = search_module.search(input);
        System.out.println(search_result);
      } catch (IOException e)
      {
        System.err.println("Il y a une erreur dans la requête, la requête vers DBPédia a échoué : " + e.getMessage());
      }
    }
  }
}

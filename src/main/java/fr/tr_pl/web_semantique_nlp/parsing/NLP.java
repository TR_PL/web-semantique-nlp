package fr.tr_pl.web_semantique_nlp.parsing;

import edu.emory.mathcs.nlp.component.template.node.NLPNode;
import edu.emory.mathcs.nlp.decode.NLPDecoder;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class NLP
{
  
  private NLPDecoder decoder;
  
  /**
   * Wrapper de la classe NLPDecoder.
   */
  public NLP()
  {
    String config = "<configuration>\n" +
      "    <tsv>\n" +
      "        <column index=\"0\" field=\"form\"/>\n" +
      "    </tsv>\n" +
      "    <models>\n" +
      "        <pos>en-pos.xz</pos>\n" +
      "        <ner>en-ner.xz</ner>\n" +
      "        <dep>en-dep.xz</dep>\n" +
      "    </models>\n" +
      "</configuration>";
    InputStream config_stream = new ByteArrayInputStream(config.getBytes());
    this.decoder = new NLPDecoder(config_stream);
  }
  
  /**
   * Fonction wrapper de la fonction de NLPDecoder.decode().
   *
   * @param text String Texte à analyser.
   * @return NLPNode[] Liste des noeuds du texte.
   */
  public NLPNode[] decode(String text)
  {
    return decoder.decode(text);
  }
  
}

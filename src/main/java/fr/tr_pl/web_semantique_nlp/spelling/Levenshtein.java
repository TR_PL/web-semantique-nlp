package fr.tr_pl.web_semantique_nlp.spelling;

import java.util.Arrays;

public class Levenshtein {
  
  /**
   * Permet de calculer la distance de Levenshtein entre deux mots.
   *
   * @param x Mot 1 a comparer.
   * @param y Mot 2 a comparer.
   * @return Renvoie la distance de Levenshtein entre les deux mots.
   */
    public static int calculate(String x, String y) {
        int[][] dp = new int[x.length() + 1][y.length() + 1];

        for (int i = 0; i <= x.length(); i++) {
            for (int j = 0; j <= y.length(); j++) {
                if (i == 0) {
                    dp[i][j] = j;
                }
                else if (j == 0) {
                    dp[i][j] = i;
                }
                else {
                    dp[i][j] = min(dp[i - 1][j - 1]
                          + costOfSubstitution(x.charAt(i - 1), y.charAt(j - 1)),
                       dp[i - 1][j] + 1,
                       dp[i][j - 1] + 1);
                }
            }
        }

        return dp[x.length()][y.length()];
    }
  
  /**
   * Permet de calculer le cout de substitution entre deux lettres.
   *
   * @param a Caractère 1 a comparer.
   * @param b Caractère 2 a comparer.
   * @return int 0 si les deux caractères sont identiques, 2 sinon.
   */
  public static int costOfSubstitution(char a, char b) {
        return a == b ? 0 : 2;
    }
    
    /**
     * Permet de renvoyer le int minimum entre plusieurs nombres.
     * @param numbers Nombres à comparer
     * @return int Nombre minimum dans l'entrée.
     */
    public static int min(int... numbers) {
        return Arrays.stream(numbers)
           .min().orElse(Integer.MAX_VALUE);
    }
}

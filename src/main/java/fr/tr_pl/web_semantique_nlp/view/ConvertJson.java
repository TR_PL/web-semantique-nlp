package fr.tr_pl.web_semantique_nlp.view;

import org.json.JSONArray;
import org.json.JSONObject;

public class ConvertJson
{
  
  private String noResult = "No result found";
  
  /**
   * Permet de vérifier si l'argument de la fonction est un numérique (si on peut le parser en double ici)
   *
   * @param strNum String chaine à vérifier
   * @return bool Vrai si numérique, Faux sinon.
   */
  public static boolean isNumeric(String strNum)
  {
    try
    {
      double d = Double.parseDouble(strNum);
    } catch (NumberFormatException | NullPointerException nfe)
    {
      return false;
    }
    return true;
  }
  
  /**
   * Permet de chercher dans le json d'une entité si un objet abstract et wikiId existe, et génère un String a afficher en conséquence.
   * @param json JSONArray JSON renvoyé par DBPédia.
   * @return String Réponse à la question de l'utilisateur.
   */
  public String searchEntite(JSONArray json)
  {
    if (json.length() == 0)
    {
      return this.noResult;
    }
    JSONObject line = json.getJSONObject(0);
    String     res  = "";
    
    if (line.has("abstract"))
    {
      res += "\n" + line.getJSONObject("abstract").getString("value");
    }
    if (line.has("wikiId"))
    {
      res += "\nHere is the Wikipedia link : https://en.wikipedia.org/w/index.php?curid=" + line.getJSONObject("wikiId").getString("value");
    }
    
    return res;
  }
  
  /**
   * Permet de chercher dans le json d'une entité si le prédicat recherché (noté ?res dans la requête) et son wikiId existe, et génère un String a afficher en conséquence.
   * @param json JSONArray JSON renvoyé par DBPédia.
   * @return String Réponse à la question de l'utilisateur.
   */
  public String searchWithPredicat(JSONArray json)
  {
    if (json.length() == 0)
    {
      return this.noResult;
    }
    JSONObject line = json.getJSONObject(0);
    String     res  = "";
    
    if (line.has("res"))
    {
      res += "\n" + formatValue(line.getJSONObject("res").getString("value"));
    }
    if (line.has("wikiId"))
    {
      res += "\nHere is the Wikipedia link : https://en.wikipedia.org/w/index.php?curid=" + line.getJSONObject("wikiId").getString("value");
    }
    return res;
  }
  
  /**
   * Permet de retirer l'URL entière d'une valeur d'un triplet de DBPédia et garder seulement la fin. (commme http://dbpedia.org/resource/France --> France)
   * @param value String URL de l'objet DBPédia
   * @return String Pointeur du lien.
   */
  protected String formatValue(String value)
  {
    String[] split = value.split("/");
    return split[split.length - 1].replace("_", " ");
  }
  
  /**
   * Permet de chercher dans le json d'une entité si une localisation (latitude, longitude) et son wikiId existe, et génère un String a afficher en conséquence.
   * @param json JSONArray JSON renvoyé par DBPédia.
   * @return String Réponse à la question de l'utilisateur.
   */
  public String searchLocation(JSONArray json)
  {
    if (json.length() == 0)
    {
      return this.noResult;
    }
    
    int        zoom = 6;
    JSONObject line = json.getJSONObject(0);
    String     res  = "";
    
    if (line.has("country"))
    {
      zoom = 15;
      res += "\nIs located in " + formatValue(line.getJSONObject("country").getString("value"));
    }
    
    if (line.has("lng") && isNumeric(line.getJSONObject("lng").getString("value")) && line.has("lat") && isNumeric(line.getJSONObject("lat").getString("value")))
    {
      
      float lng = Float.parseFloat(line.getJSONObject("lng").getString("value"));
      if (line.has("lngew") && lng > 0 && line.getJSONObject("lngew").getString("value").equals("W"))
      {
        lng = -1 * lng;
      }
      
      res += "\nLocation on Google Map : https://www.google.com/maps/@" + line.getJSONObject("lat").getString("value") + "," + lng + "," + zoom + "z";
    }
    
    if (res.length() == 0)
    {
      return this.noResult;
    }
    
    return res;
  }
  
}

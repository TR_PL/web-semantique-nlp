package fr.tr_pl.web_semantique_nlp.search;

import edu.emory.mathcs.nlp.component.template.node.NLPNode;
import fr.tr_pl.web_semantique_nlp.parsing.NLP;
import fr.tr_pl.web_semantique_nlp.storages.DBPedia;
import fr.tr_pl.web_semantique_nlp.view.ConvertJson;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Search
{
  private NLP nlp;
  
  public Search()
  {
    this.nlp = new NLP();
  }
  
  /**
   * Fonction principale qui permet d'effectuer une recherche
   *
   * @param input String Phrase de la recherche (Ex: Who is Steve Jobs ? What is the population of France ?)
   * @return String Retourne un String qui contient la réponse à la question
   * @throws IOException Éxecutée lorsque DBPédia est indisponible.
   */
  public String search(String input) throws IOException
  {
    Pattern p = Pattern.compile("-");
    
    NLPNode save_n = null;
    
    NLPNode[] nodes = nlp.decode(input);//"Who is Steve Jobs ?" Who is Steve Jobs ? live in Paris in France ?
    
    List<String> entities = new ArrayList<String>();
    
    boolean NNP_active = false;
    
    for (NLPNode n : nodes)
    {
      //System.out.println(n);
      
      if (!n.getPartOfSpeechTag().equals("NNP") && NNP_active)
      {
        break;
      }
      
      Matcher m = p.matcher(n.getNamedEntityTag());
      if (!n.getNamedEntityTag().equals("O") && m.find() && !n.getLemma().equals("where"))
      {
        p = Pattern.compile(n.getNamedEntityTag().split("-")[1]);
        save_n = n;
        NLPNode dependency = n;
        boolean valid      = true;
        while (valid)
        {
          entities.add(dependency.getWordForm());
          dependency = dependency.getDependencyHead();
          
          m = p.matcher(dependency.getNamedEntityTag());
          valid = m.find();
          if (valid)
          {
            save_n = dependency;
          }
        }
        
        break;
      } else if (n.getPartOfSpeechTag().equals("NNP") && !n.getLemma().equals("where"))
      {//Si c'est un nom propre
        entities.add(n.getWordForm());
        save_n = n;
      }
    }
    
    String search_object = null;
    //Recherche s'il y a un "of" ou un "'s"
    if (save_n != null)
    {
      if (save_n.getDependencyHead().getLemma().equals("of"))
      {//S'il y a une relation avec "of"
        NLPNode of = save_n.getDependencyHead();
        search_object = of.getDependencyHead().getWordForm();
      } else if (save_n.getDependencyHead().getPartOfSpeechTag().equals("JJ") || save_n.getDependencyHead().getPartOfSpeechTag().equals("NN"))
      {//S'il y a une relation avec l'entité
        search_object = save_n.getDependencyHead().getWordForm();
      }
    }
    //System.out.println(search_object);
    
    
    ConvertJson convert      = new ConvertJson();
    String      search       = String.join(" ", entities);
    DBPedia     dbPedia      = new DBPedia();
    String      return_value = "";
    
    p = Pattern.compile("who('| )");
    Matcher m = p.matcher(input.toLowerCase());
    if (m.find())
    {
      if (search_object != null)
      {
        //System.out.println("Search : " + search_object + " of " + search);
        String predicat = dbPedia.searchGoodPredicat(search, search_object);
        return_value = convert.searchWithPredicat(DBPedia.searchWithPredicat(search, predicat).getJSONObject("results").getJSONArray("bindings"));
      } else
      {
        //System.out.println("Search : " + search);
        JSONObject searchPerson = DBPedia.searchPerson(search);
        return_value = convert.searchEntite(searchPerson.getJSONObject("results").getJSONArray("bindings"));
      }
    }
    p = Pattern.compile("what('| )");
    m = p.matcher(input.toLowerCase());
    if (m.find())
    {
      if (search_object != null)
      {
        //System.out.println("Search : " + search_object + " of " + search);
        String predicat = dbPedia.searchGoodPredicat(search, search_object);
        return_value = convert.searchWithPredicat(DBPedia.searchWithPredicat(search, predicat).getJSONObject("results").getJSONArray("bindings"));
      } else
      {
        //System.out.println("Search : " + search);
        JSONObject searchEntitie = DBPedia.searchWhat(search);
        return_value = convert.searchEntite(searchEntitie.getJSONObject("results").getJSONArray("bindings"));
      }
    }
    
    p = Pattern.compile("where('| )");
    m = p.matcher(input.toLowerCase());
    if (m.find())
    {
      //System.out.println("Search : " + search);
      JSONObject searchLocation = DBPedia.searchWhere(search);
      return_value = convert.searchLocation(searchLocation.getJSONObject("results").getJSONArray("bindings"));
    }
    return return_value;
  }
  
}

package fr.tr_pl.web_semantique_nlp.storages;

import fr.tr_pl.web_semantique_nlp.spelling.Levenshtein;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DBPedia
{
  
  private Map<String, String> predicat_correspondance;
  
  /**
   * Permet d'initialiser les variables nécéssaires au requêtage.
   */
  public DBPedia()
  {
    this.predicat_correspondance = new HashMap<String, String>();
    this.predicat_correspondance.put("president", "leader");
  }
  
  /**
   * Requête permettant de chercher une personne.
   *
   * @param person String Personne à rechercher.
   * @return JSONObject Objet JSON qui contient la réponse de DBPédia.
   * @throws IOException Si envoyée, site incassessible.
   */
  public static JSONObject searchPerson(String person) throws IOException
  {
    String query = "select DISTINCT ?person ?wikiId ?abstract where { VALUES ?name {'" + person + "'@en '" + person + "'@fr} ?person rdfs:label ?name; dbo:wikiPageID ?wikiId; a foaf:Person; dbo:abstract ?abstract FILTER (lang(?abstract) = 'en' || lang(?abstract) = 'fr')} LIMIT 1";
    return sendQuery(query);
  }
  
  /**
   * Prépare une requête pour la recherche d'un prédicat.
   *
   * @param entity   String Entité à rechercher.
   * @param predicat String Prédicat associé à l'entité. (Exemple : "fils" ou "président").
   * @return JSONObject Objet JSON qui contient la réponse de DBPédia.
   * @throws IOException Si envoyée, site incassessible.
   */
  public static JSONObject searchWithPredicat(String entity, String predicat) throws IOException
  {
    String query = "select DISTINCT ?entite ?wikiId ?res where { VALUES ?name {'" + entity + "'@en '" + entity + "'@fr} ?entite rdfs:label ?name; dbo:wikiPageID ?wikiId; <" + predicat + "> ?res} LIMIT 10";
    return sendQuery(query);
  }
  
  /**
   * Méthode Statique retournant un objet JSON qui contient la réponse à la requête vers DBPédia
   *
   * @param query String Requête à envoyer à fr.tr_pl.web_semantique_nlp.storages.DBPedia
   * @return JSONObject Objet JSON qui contient la réponse.
   * @throws IOException Si envoyée, site incassessible.
   */
  public static JSONObject sendQuery(String query) throws IOException
  {
    
    //System.out.println("\n Query : " + query);
    query = URLEncoder.encode(query, "UTF-8");
    String url_path = "http://dbpedia.org/sparql?default-graph-uri=http%3A%2F%2Fdbpedia.org&query=" + query + "&format=application%2Fsparql-results%2Bjson";
    
    //System.out.println("\n URL Query : " + url_path);
    URL url = new URL(url_path);
    
    ArrayList<String> lines = new ArrayList<>();
    try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), StandardCharsets.UTF_8)))
    {
      for (String line ; (line = reader.readLine()) != null ; )
      {
        if (!line.equals(""))
        {
          lines.add(line.trim());
        }
      }
    }
    String page = String.join("", lines);
    return new JSONObject(page);
  }
  
  /**
   * Permet d'effectuer la recherche du "What".
   *
   * @param search String Entité à rechercher.
   * @return JSONObject Objet JSON qui contient la réponse de DBPédia.
   * @throws IOException Si envoyée, site incassessible.
   */
  public static JSONObject searchWhat(String search) throws IOException
  {
    String query = "select ?entitie ?wikiId ?abstract where { VALUES ?name {'" + search + "'@en '" + search + "'@fr} ?entitie rdfs:label ?name; dbo:wikiPageID ?wikiId; dbo:abstract ?abstract FILTER (lang(?abstract) = 'en' || lang(?abstract) = 'fr')} LIMIT 1";
    return sendQuery(query);
  }
  
  /**
   * Permet d'effectuer la recherche du "Where".
   *
   * @param search String Entité à rechercher.
   * @return JSONObject Objet JSON qui contient la réponse de DBPédia.
   * @throws IOException Si envoyée, site incassessible.
   */
  public static JSONObject searchWhere(String search) throws IOException
  {
    String query = "select ?entitie ?p ?o ?country ?lat ?lng ?lngew where { VALUES ?name {'" + search + "'@en '" + search + "'@fr} ?entitie rdfs:label ?name. OPTIONAL {?entitie (dbo:location|dbo:country) ?country.}. OPTIONAL {?entitie geo:long|dbp:longitude|dbp:longd? ?lng.}. OPTIONAL {?entitie geo:lat|dbp:latitude|dbp:latd? ?lat.}. OPTIONAL {?entitie dbp:longew ?lngew.}. ?entitie ?p ?o. ?entitie a ?y. } LIMIT 10";
    return sendQuery(query);
  }
  
  /**
   * Permet d'effectuer la recherche avec un prédicat en modifiant les prédicats inconnues (comme "president" par "leader")
   *
   * @param search   String Entité à rechercher.
   * @param predicat Prédicat associé à l'entité à rechercher.
   * @return JSONObject Objet JSON qui contient la réponse de DBPédia.
   * @throws IOException Si envoyée, site incassessible.
   */
  public String searchGoodPredicat(String search, String predicat) throws IOException
  {
    
    if (this.predicat_correspondance.containsKey(predicat))
    {
      predicat = this.predicat_correspondance.get(predicat);
    }
    
    String     query = "select DISTINCT ?entitie ?predicat where { VALUES ?name {'" + search + "'@en '" + search + "'@fr} ?entitie rdfs:label ?name; dbo:wikiPageID ?wikiId; dbo:abstract ?abstract; ?predicat ?x }";
    JSONObject res   = sendQuery(query);
    //System.out.println(res);
    
    int    lv_score = 1000;
    String save     = "";
    
    //On cherche qu'elle prédicat se rapproche de notre recherche
    for (int i = 0 ; i < res.getJSONObject("results").getJSONArray("bindings").length() ; i++)
    {
      JSONObject line    = res.getJSONObject("results").getJSONArray("bindings").getJSONObject(i);
      String     url     = line.getJSONObject("predicat").getString("value");
      String[]   explode = url.split("/");
      String     tmp     = explode[explode.length - 1].toLowerCase();
      if (tmp.equals(predicat))
      {
        return url;
      }
      
      int cal = Levenshtein.calculate(tmp, predicat);
      if (cal < lv_score)
      {
        lv_score = cal;
        save = url;
      }
    }
    
    if (lv_score <= 8)
    {
      return save;
    }
    return "";
  }
}

package fr.tr_pl.web_semantique_nlp.spelling;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LevenshteinTest
{
  
  @Test
  void calculate()
  {
    int expected_value1 = 1;
    int result_value1   = Levenshtein.calculate("chien", "chiens");
    assertEquals(expected_value1, result_value1);
    
    int expected_value2 = 5;
    int result_value2   = Levenshtein.calculate("chien", "chat");
    System.out.println(result_value2);
    assertEquals(expected_value2, result_value2);
    
    int expected_value3 = 0;
    int result_value3   = Levenshtein.calculate("chien", "chien");
    assertEquals(expected_value3, result_value3);
  }
  
  @Test
  void costOfSubstitution()
  {
    assertEquals(0, Levenshtein.costOfSubstitution('A', 'A'));
    assertEquals(0, Levenshtein.costOfSubstitution('B', 'B'));
    assertEquals(0, Levenshtein.costOfSubstitution('C', 'C'));
    assertEquals(2, Levenshtein.costOfSubstitution('A', 'B'));
    assertEquals(2, Levenshtein.costOfSubstitution('A', 'C'));
    assertEquals(2, Levenshtein.costOfSubstitution('A', 'D'));
  }
  
  @Test
  void min()
  {
    int expected_value1 = 1;
    int result_value1   = Levenshtein.min(1, 2, 3, 4, 5, 6, 1);
    assertEquals(expected_value1, result_value1);
    
    int expected_value2 = 1029;
    int result_value2   = Levenshtein.min(1029, 20000);
    assertEquals(expected_value2, result_value2);
    
    int expected_value3 = 1;
    int result_value3   = Levenshtein.min(1);
    assertEquals(expected_value3, result_value3);
  }
}
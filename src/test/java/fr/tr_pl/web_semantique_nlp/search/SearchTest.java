package fr.tr_pl.web_semantique_nlp.search;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SearchTest
{
  
  private final Search search_module;
  
  private SearchTest()
  {
    this.search_module = new Search();
  }
  
  @Test
  void search()
  {
    try
    {
      String search_result         = search_module.search("Who is Steve Jobs ?");
      String value_expected = "\nSteven Paul Jobs, dit Steve Jobs, (San Francisco, 24 février 1955 - Palo Alto, 5 octobre 2011) est un entrepreneur et inventeur américain, souvent qualifié de visionnaire, et une figure majeure de l'électronique grand public, notamment pionnier de l'avènement de l'ordinateur personnel, du baladeur numérique, du smartphone et de la tablette tactile. Cofondateur, directeur général et président du conseil d'administration d'Apple Inc, il dirige aussi les studios Pixar et devient membre du conseil d'administration de Disney lors du rachat en 2006 de Pixar par Disney. Steve Jobs, Steve Wozniak et Ronald Wayne créent Apple le 1er avril 1976 à Cupertino. Au début des années 1980, Steve Jobs saisit le potentiel commercial des travaux du Xerox Parc sur le couple interface graphique/souris, ce qui conduit à la conception du Lisa, puis du Macintosh en 1984, les premiers ordinateurs grand public à profiter de ces innovations. Après avoir perdu une lutte de pouvoir à la tête d'Apple avec le directeur général qu'il avait pourtant recruté, John Sculley, il quitte l'entreprise en septembre 1985 pour fonder NeXT. En 1986, il rachète la division Graphics Group de Lucasfilm, la transforme en Pixar Animation Studios et rencontre le succès commercial en 1995 avec Toy Story, un film dont il est le producteur délégué. Il reste directeur-général propriétaire de la compagnie (à 50,1 %) jusqu'à son acquisition par la Walt Disney Company en 2006. Début 1997, Apple, alors au bord de la faillite, rachète NeXT. L'opération permet à Steve Jobs de revenir à la tête de la firme qu'il a cofondée et fournit à Apple le code source de NeXTSTEP à partir duquel est développé le système d'exploitation Mac OS X. Il supervise durant les quatorze années suivantes la création, le lancement et le développement de l'iMac (1998), de l'iPod, d'iTunes et de la chaîne de magasins Apple Store (2001), de l'iTunes Store (2003), de l'iPhone (2007) et de l'iPad (2010), présentant les différents produits à un rythme pluriannuel lors de ses fameuses keynotes et faisant de son entreprise une des plus riches au monde au moment de sa mort. En 2003, Steve Jobs apprend qu'il est atteint d'une forme rare de cancer pancréatique. Il passe les années suivantes à lutter contre la maladie, subissant plusieurs hospitalisations et arrêts de travail, apparaissant de plus en plus amaigri au fur et à mesure que sa santé décline. Il meurt le 5 octobre 2011 à son domicile de Palo Alto, à l'âge de cinquante-six ans. Sa mort soulève une importante vague d'émotion à travers le monde.\n" + "Here is the Wikipedia link : https://en.wikipedia.org/w/index.php?curid=7412236";
      assertEquals(value_expected, search_result);
      
      String search_result2         = search_module.search("What is Russia?");
      String value_expected2 = "\nRussia (/ˈrʌʃə/; Russian: Росси́я, tr. Rossija; IPA: [rɐˈsʲijə]; from the Greek: Ρωσία — Rus'), also officially known as the Russian Federation (Russian: Росси́йская Федера́ция, tr. Rossijskaja Federacija; IPA: [rɐˈsʲijskəjə fʲɪdʲɪˈratsɨjə]), is a transcontinental country in Eurasia. At 17,075,200 square kilometres (6,592,800 sq mi), Russia is the largest country in the world, covering more than one eighth of Earth's inhabited land area, and the ninth most populous, with over 146.6 million people at the end of March 2016. Extending across the entirety of northern Asia and much of Eastern Europe, Russia spans eleven time zones and incorporates a wide range of environments and landforms. From northwest to southeast, Russia shares land borders with Norway, Finland, Estonia, Latvia, Lithuania and Poland (both with Kaliningrad Oblast), Belarus, Ukraine, Georgia, Azerbaijan, Kazakhstan, China, Mongolia, and North Korea. It shares maritime borders with Japan by the Sea of Okhotsk and the U.S. state of Alaska across the Bering Strait. The nation's history began with that of the East Slavs, who emerged as a recognizable group in Europe between the 3rd and 8th centuries AD. Founded and ruled by a Varangian warrior elite and their descendants, the medieval state of Rus arose in the 9th century. In 988 it adopted Orthodox Christianity from the Byzantine Empire, beginning the synthesis of Byzantine and Slavic cultures that defined Russian culture for the next millennium. Rus' ultimately disintegrated into a number of smaller states; most of the Rus' lands were overrun by the Mongol invasion and became tributaries of the nomadic Golden Horde in the 13th century. The Grand Duchy of Moscow gradually reunified the surrounding Russian principalities, achieved independence from the Golden Horde, and came to dominate the cultural and political legacy of Kievan Rus'. By the 18th century, the nation had greatly expanded through conquest, annexation, and exploration to become the Russian Empire, which was the third largest empire in history, stretching from Poland on the west to Alaska on the east. Following the Russian Revolution, the Russian Soviet Federative Socialist Republic became the largest and leading constituent of the Union of Soviet Socialist Republics, the world's first constitutionally socialist state. The Soviet Union played a decisive role in the Allied victory in World War II, and emerged as a recognized superpower and rival to the United States during the Cold War. The Soviet era saw some of the most significant technological achievements of the 20th century, including the world's first human-made satellite and the launching of the first humans in space. By the end of 1990, the Soviet Union had the world's second largest economy, largest standing military in the world and the largest stockpile of weapons of mass destruction. Following the partition of the Soviet Union in 1991, fourteen independent republics emerged from the USSR; as the largest, most populous, and most economically developed republic, the Russian SFSR reconstituted itself as the Russian Federation and is recognized as the continuing legal personality and sole successor state of the Soviet Union. It is governed as a federal semi-presidential republic. The Russian economy ranks as the twelfth largest by nominal GDP and sixth largest by purchasing power parity in 2015. Russia's extensive mineral and energy resources are the largest such reserves in the world, making it one of the leading producers of oil and natural gas globally. The country is one of the five recognized nuclear weapons states and possesses the largest stockpile of weapons of mass destruction. Russia is a great power and a permanent member of the United Nations Security Council, as well as a member of the G20, the Council of Europe, the Asia-Pacific Economic Cooperation (APEC), the Shanghai Cooperation Organisation (SCO), the Organization for Security and Co-operation in Europe (OSCE), and the World Trade Organization (WTO), as well as being the leading member of the Commonwealth of Independent States (CIS), the Collective Security Treaty Organization (CSTO) and one of the five members of the Eurasian Economic Union (EEU), along with Armenia, Belarus, Kazakhstan, and Kyrgyzstan.\n" + "Here is the Wikipedia link : https://en.wikipedia.org/w/index.php?curid=25391";
      assertEquals(value_expected2, search_result2);
      
      
      String search_result3 = search_module.search("Steve Jobs");
      String value_expected3 = "";
      assertEquals(value_expected3, search_result3);
  
      String search_result4         = search_module.search("What is the population of France ?");
      String value_expected4 = "\n64590000\n" + "Here is the Wikipedia link : https://en.wikipedia.org/w/index.php?curid=5843419";
      assertEquals(value_expected4, search_result4);
      
      String search_result5  = search_module.search("Who's the child of Steve Jobs  ?");
      String value_expected5 = "\nLisa Brennan-Jobs\n" + "Here is the Wikipedia link : https://en.wikipedia.org/w/index.php?curid=7412236";
      assertEquals(value_expected5, search_result5);
      
      
    } catch (IOException e)
    {
      e.printStackTrace();
    }
  }
}
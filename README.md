# Readme

Pour éxecuter le programme depuis le dossier contenant `src` et `test` il suffit d'éxecuter la commande :
```bash
mvn install && mvn exec:java -D exec.mainClass=fr.tr_pl.web_semantique_nlp.Main
``` 

Pour lancer les tests il faut lancer la commande 
```bash
mvn test
```


Réalisé par Tristan LEROUX et Pierre LEBOUC